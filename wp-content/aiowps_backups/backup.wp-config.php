<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpstagey_admin' );

/** MySQL database username */
define( 'DB_USER', 'wpstagey_user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'bZMDuwOQG7NH' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R$>`g{eF@fe[htCB1ST+?!Q|)m1Lv>U}r7`GQ$.=_S3 T+,3rk3E~YG&rEzf(IJ4' );
define( 'SECURE_AUTH_KEY',  '{x(z1k*oF480/X(1@9N[Gj[uu54dM@];KAMek7C16[bHC0b4B{ {6?)/nSXh1#PF' );
define( 'LOGGED_IN_KEY',    '!z3i{A!2FzQE=S|g,!b+0/^t5W15:FQ4tZASu>`+#zT(2.~X(/F}Q.@J1A Z*R*P' );
define( 'NONCE_KEY',        'nWSK*|r=We{^DZ]5~@7v<$wrkI8*{7X4gi&z8EIk@:T]@XibNV~;i?ot5koC_dC:' );
define( 'AUTH_SALT',        'oX[Mtx#rNP[E8Sbm<IM{x?;lmTf!z;8;B%z(;6,,,bU!N]=f1`r#1%#I$a3;R~85' );
define( 'SECURE_AUTH_SALT', '%H`_6Ix-VsJv}3+)5R{:Y^~aaBeIk<jl>1gCWxA|o;tr<H#wp^@?h2&/sA#G3Zu7' );
define( 'LOGGED_IN_SALT',   '#Pl65V~2[3Q%q8>7j+=m8S2Z1jy4H78rj2a4Ux!{d@&g*DX_cV(WD4Cg50v*jm(b' );
define( 'NONCE_SALT',       ')vCeuV!0qXjkKIpC}p@Z5LW#n;FieU5c=`k6$K>^@e]Jq=qrHb%tIGJE#D(B~8b$' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'yel_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
